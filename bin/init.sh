#!/bin/sh

CHECK_FILE="/tmp/letsencrypt/live/keyoxide/fullchain.pem"
SLEEP=1
TIMEOUT=30

export HTTP_PORT=$(echo $HTTP_PORT_MAP | cut -d: -f2)
export HTTPS_PORT=$(echo $HTTPS_PORT_MAP | cut -d: -f2)

defined_envs=$(printf '${%s} ' $(env | cut -d= -f1))

envsubst "${defined_envs}" < /tmp/default.ssl.conf.tmpl > /tmp/default.ssl.conf
envsubst "${defined_envs}" < /tmp/default.conf.tmpl > /tmp/nginx-conf.d/default.conf

until [ -f "${CHECK_FILE}" ]
do
   sleep ${SLEEP}
   COUNTER=$((${COUNTER}+1))
   echo "Checking if SSL file exist[${COUNTER}/${TIMEOUT} retries]"
   if [ "${COUNTER}" -eq "${TIMEOUT}" ]; then
       echo "Timeout reached. SSL cert not found. Exiting"
       exit 1
   fi
done && cp /tmp/default.ssl.conf /tmp/nginx-conf.d/default.ssl.conf || echo exit 1

echo "SSL files exist (may need to restart docker containers)" && exit 0
